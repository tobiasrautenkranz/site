changelog := _includes/changelog.html.fragment

all: $(changelog) images
	bundle exec jekyll build -q

REPO := https://gitlab.com/tobiasrautenkranz/site/commit/

.PHONY: $(changelog)
$(changelog):
	@echo "<dl>" > $@
	@git log --pretty=format:'<dt>%n<a href="$(REPO)%H">%ai</a>%n</dt><dd>%s</dd>%n' >> $@
	@echo "</dl>" >> $@

# serve locally
serve:
	bundle exec jekyll serve -B -D > /dev/null 2>&1

kill:
	killall jekyll

serve-fg:
	bundle exec jekyll serve -D

# images
img/%.png: img/%.svg
	inkscape --without-gui --export-png=$@ --export-dpi=70 $< > /dev/null
	optipng -quiet $@

# images to be built
image_objects :=
include image_objects.mk
image_objects_full_path := $(addprefix img/, $(image_objects))

.PHONY: images
images: $(image_objects_full_path)

# clean
.PHONY: clean
clean:
	rm -f $(changelog)

.PHONY: dist-clean
dist-clean: clean
	bundle exec jekyll clean
	rm -f $(image_objects_full_path)

# lint
mld_rules := "~MD002,~MD012,~MD033"

.PHONY: lint
lint:
	-mdl -r "$(mld_rules)" _posts/*.md
	-mdl -r "$(mld_rules)" _drafts/*.md

.PHONY: lint-drafts
lint-drafts:
	-mdl -r "$(mld_rules)" _drafts/*.md
