---
layout: post
title: Hokkaido Milchbrot 北海道ミルクパン
lang: de
category: recipes
tags: recipe bread
---
Japanisches Milchbrot ist extra weich und luftig und somit immer schnell gegessen.

![Milchbrot]({{ "/img/milkbread/crumb.jpg" | relative_url }})

## Tangzhong

Der Tangzhong ist ein Vorteig, welcher diesem Brot sein besonderes fluffige
Krume verleiht.

![Tangzhong]({{ "/img/milkbread/tangzhong.jpg" | relative_url }})

### Zutaten

| 200 g | Milch|
| 50 g  | Weissmehl |

Die Milch mit dem Mehl mischen und unter gelegentlichem Rühren erhitzen bis
sich eine Paste gebildet hat.

## Milchbrot

<table>
  <caption>Zutaten</caption>
  <tr>
    <td>950 g</td>
    <td>Weissmehl</td>
  </tr>
  <tr>
    <td>600 g &ndash; 650 g</td>
    <td>Milch</td>
  </tr>

  <tr>
    <td></td>
    <td>Tangzhong (Abgekühlt)</td>
  </tr>

  <tr>
    <td>7 g</td>
    <td>Trockenhefe</td>
  </tr>

  <tr>
    <td>1</td>
    <td>Ei</td>
  </tr>
  <tr>
    <td>1</td>
    <td>Eiklar (Eigelb beiseite stellen)</td>
  </tr>

  <tr>
    <td>20 g</td>
    <td>Salz</td>
  </tr>
  <tr>
    <td>~30 g</td>
    <td>Zucker</td>
  </tr>
  <tr>
    <td>~50 g</td>
    <td>Butter</td>
  </tr>
  <tr>
    <td>~20 g</td>
    <td>Milchpulver (Optional)</td>
  </tr>
</table>

Alle Zutaten bis auf das Salz und die Butter zusammenmischen
und 30 Minuten abgedeckt in einer Schüssel ruhen lassen.

Das Salz zugeben und den Teig gut kneten. Die Butter währen dem Kneten
stückweise beigeben.
Der Teig ist zu beginn klebrig und muss entsprechen intensiv geknetet werden,
um die richtige Konsistenz zu erhalten.

Den Teig in einer Schüssel ca. 2 h gehen lassen,
bis das Volumen sich verdoppelt hat.

### Formen

![Geformtes Brot]({{ "/img/milkbread/pan.jpg" | relative_url}})
> Geformtes Brot (aus 1 kg Mehl)
> Springform Durchmesser: 17 cm Höhe: 6 cm; 9 &times; 75 g Teig
> Cakeform 9 cm &times; 35 cm Höhe: 7 cm

Den gegangenen Teig aus der Schüssel nehmen und etwas flach klopfen,
um grosse Blasen zu zerstören.
10 Minuten ruhen lassen und anschliessend in Stücke teilen.
Als Rolle oder Kugeln in einer gebutterten Form platzieren.

_Tipp_: Teigstücke abwägen, um ein gleichmässiges Brot zu erhalten.

### Backen

![Bereit für den Ofen]({{ "/img/milkbread/rise.jpg" | relative_url}})

Das Brot in der Form ca. 2 h gehen lassen.
Anschliessend mit dem Eigelb bestreichen und im
vorgeheizten Ofen bei 200 °C etwa 40 Minuten backen
(bis der gewünschte Bräunegrad erreicht ist).

Während der ersten 10 Minuten kann mit einer Spritzflasche Wasser
auf die Innenwände des Ofens gesprüht werden, damit der Teig besser geht.

Brot aus dem Ofen und der Form nehmen und auf einem Gitter abkühlen lassen.
Schmeckt leicht warm am besten.

![Milchbrot]({{ "/img/milkbread/bread.jpg" | relative_url }})

## Links

* [_Hokkaido Milk Bread with Tangzhong_](
  http://www.thefreshloaf.com/node/32997/hokkaido-milk-bread-tangzhong).
  Floydm. The Fresh Loaf. April 10, 2013
* [_Hokkaido Milk Bread Recipe (北海道ミルクパン)_](
  http://www.mensoregirl.com/hokkaido-milk-bread/).
  Mensore Girl. February 1, 2015
