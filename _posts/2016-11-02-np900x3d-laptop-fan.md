---
layout: post
title: NP900X3D Laptop Fan
date: 2016-11-02 02:45:00
category: hw
tags: hardware
---
The fans of my
[Samsung NP900X3D Laptop](http://www.samsung.com/us/computer/pcs/NP900X3D-A01US-specs)
where starting to make quite some noise.

This was fixed by oiling the axle of the two fans. Due to slight damage to the
fan case on disassembly, some tape was required (seen in red).

![disassembled fan]({{ "/img/np900/fan.jpg" | relative_url }})
![fixed & installed fan]({{ "/img/np900/fixed.jpg" | relative_url }})
