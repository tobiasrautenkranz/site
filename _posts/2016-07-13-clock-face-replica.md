---
layout: post
title: Clock Face Replica
date: 2016-07-13 15:55:48
category: hw
tags: hardware
---
Construction of a clock face replica for grandmas clock.

The Faux-Emaille clock face was in bad shape. The clear plastic layer was separating
from the paper layer below and badly stained. For the reconstruction
the clock face was removed from the clock. The two layers were completely
separated and then scanned using a flatbed scanner
(the scans:
[layer 1]({{ "/img/clock/clock_face_raw.png" | relative_url }} )
[layer 2]({{ "/img/clock/clock_face_raw_layer_2.png" | relative_url }} )
).

After digitally superimposing the scans and some cleanup,
the [result]({{ "/img/clock/clock_face.png" | relative_url }})
was traced in [Inkscape](https://inkscape.org).
The resulting [svg]({{ "/img/clock/clock_face.svg" | relative_url }} )
was printed on light, glossy paper using a Laserprinter.

To install the new face, the old paper layer was destructively removed
from the metal plate.
The three holes were cut in the newly printed paper.
Then the paper was slightly wetted before glueing to
improve wrapping the curved metal plate.
After trimming the borders the new clock face received
several coatings with a varnish spray and then was installed in the cleaned clock.

![reconstructed clock]({{ "/img/clock/post.jpg" | relative_url }})


## downloads

* [clock face.svg]({{ "/img/clock/clock_face.svg" | relative_url }} )
