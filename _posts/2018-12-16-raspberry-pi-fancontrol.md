---
layout: post
title: Temperature Based Fan Control for the Raspberry Pi
lang: en
category: code
tags: code hardware
---

The fan (3010S DC 5V 0.20A) I use in the
[NESPi CASE](https://www.nespi-case.ch/) with a
[Rapsberry Pi 3 Model B+](https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus/)
is quite loud.
To reduce the noise, a system to switch the fan on/off based on demand was
implemented.

![abstract]({{ "/img/raspberry-pi-fancontrol/installed.jpg" | relative_url }})


Several similar approaches have been implemented; e.g.:
[TheDrHax/rpi-fan-control](https://github.com/TheDrHax/rpi-fan-control) and
[How to control a fan to cool the CPU of your RaspBerryPi - Edo Scalafiotti](https://hackernoon.com/how-to-control-a-fan-to-cool-the-cpu-of-your-raspberrypi-3313b6e7f92c?gi=a218258fbee1)
or lowerning the supply voltage to 3.3 V:
[https://www.reddit.com/r/RetroPie/comments/80410w/nespi_fan_always_on/](https://www.reddit.com/r/RetroPie/comments/80410w/nespi_fan_always_on/)

# Control Circuit
The fan has two wires: +5 V and ground.
It is controlled with a
[GPIO pin](https://www.raspberrypi.org/documentation/usage/gpio/)
connected to a custom MOSFET circuit
supplying the fan. The used n-channel IRF3710 MOSFET requires a Gate Threshold
Voltage above the 3.3 V of the GPIO pin. Thus, a drive circuit similar to
[Fig 1. Inverting Driver](https://tahmidmc.blogspot.com/2012/12/low-side-mosfet-drive-circuits-and_23.html)
was constructed (The pull down transistor _R6_ for the transistor base was
omitted). Parts were chosen by what I had lying around.

The completed circuit:  
![circuit front]({{ "/img/raspberry-pi-fancontrol/circuit_front.jpg" | relative_url }})  
[circuit back]({{ "/img/raspberry-pi-fancontrol/circuit_back.jpg" | relative_url }})

The inverting configuration results in the default state of the fan being on.
Which theoretically is safer with respect to high temperatures and also allows the
takeover of the software control after startup to be audibly confirmed.

## Installation
A hole was drilled in the support structure of a screw hole of the case. Using
a zip tie through the MOSFET heat sink the circuit was affixed to the case.

![installation of circuit]({{ "/img/raspberry-pi-fancontrol/on_case.jpg" | relative_url }})

The circuit is connected to the 2 pin fan connector on the case circuit board
and to GPIO pin 21 on the Rapsberry Pi  with the green wire.
The fan is connected to the 2 pin header on the control circuit.

# Control Software
The software is a C++ program reading the CPU temperature from _/sys/_ and
using the [WiringPi](http://wiringpi.com/) library to control the fan with a
GPIO pin. When the on or off threshold temperature is reached, the fan is switched
on respectively off.

Due to the use of systemd for starting the program no special damonization code
is needed (see:
[New-Syle Daemons](http://0pointer.de/public/systemd-man/daemon.html#New-Style%20Daemons)).
A _.service_ file for systemd is provided.

## Pulse Width Modulation
A try with Pulse Width Modulation (PWM) had no usable success; both with
Software and Hardware Modulation. While the fan
could be slowed somewhat (not to low percentages ~30%), the modulation increased
the noise. A dedicated PWM input on the fan (4 wire connector) would probably be
needed for a successful PWM.

Contrary success using Software PWM is noted by:
[soulgriever/FanGPIO](https://github.com/soulgriever/FanGPIO) and
[GPIO en C: Control automático de ventilador según temperatura - josrios](https://descubriendolaorangepi.wordpress.com/2017/06/20/gpio-en-c-control-automatico-de-ventilador-segun-la-temperatura/)

## Source Code
The source code is at:  
[https://gitlab.com/tobiasrautenkranz/raspberry-pi-fancontrol](https://gitlab.com/tobiasrautenkranz/raspberry-pi-fancontrol)


