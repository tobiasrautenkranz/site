---
layout: post
title: Kürbiskernöl-Challa
       [חלה מיט דעם  בוימל פון די זאמען פון דעם קירבעס]
lang: de
category: recipes
tags: recipe bread
---
Eine Challa mit steirischem Kürbiskernöl – nicht authentisch und schön grün.

![Kürbiskernöl-Challa]({{ "/img/challa/challa_crumb.jpg" | relative_url }})

## Zutaten

Für vier Challot (ein Backblech) werden benötigt:

| 1 kg  | Weissmehl    | 100 %
| 700 g | Milch        |  60 %
|   7 g | Hefe         |   0.7 %
|  80 g | Butter       |   8 %
|  20 g | Salz         |   2 %
|  30 g | Zucker       |   3 %
|  70 g | Kürbiskernöl | 6 - 8 %

ausserdem zum Bestreuen/Bestreichen:

| 1x    | Eigelb       | (18 g) 2 %
| 60 g  | Kürbiskerne in Stücken | 6 %


## Herstellung

### Teig

Für den Teig die Milch in einer Schüssel mit der Hefe, dem Zucker
und dem Kürbiskernöl mischen.
Anschliessend das Weissmehl beigeben und in der Schüssel mischen,
bis sich ein homogener Teig gebildet hat.
Die Schüssel mit dem Teig zudecken und etwa 15 Minuten ruhen lassen.

Nun den Teig aus der Schüssel nehmen, gut kneten und dabei langsam den Butter
und das Salz einarbeiten.
Jetzt muss der Teig in der zugedeckten Schüssel ungefähr zwei Stunden gehen;
bis sich sein Volumen verdoppelt hat.

### Flechten

Danach den Teig aus der Schüssel nehmen und in 16 (4 × 4) Teigstücke teilen.
Im nächsten Schritt werden jeweils vier Teigstücke zu einer Challa geflochten
und auf dem Backblech platziert.

Anschaulich erklärt ist das Flechten im Video von [Julie Remer,
_braiding a round challah_, YouTube, 23. Sep, 2016
](https://youtu.be/kvbW4zMpzB4) oder siehe die folgende Beschreibung:

Zum Flechten vier Teigstücke zu etwa 30 cm langen Rollen formen.
Die vier Stränge in einem Kreuz hinlegen
und 4 bis 5 mal wie gezeigt im Kreis flechten.

<img src='{{ "/img/challa/round_challa.png" | relative_url }}'
  class="full-width" />

Die Zahlen beschreiben die Position der Stränge und nicht den Strang;
das heisst, jeder Strang hat abhängig vom Schritt eine andere Nummer.

Schritt _a_ in der Abbildung ist nur der Konsistenz der Flechtoperation
geschuldet und es empfiehlt sich die Stränge gleich wie in _1_ anzuordnen.
Anschliessend mit Schritt zwei (siehe unten) fortfahren um _2_ zu erhalten.

Die zwei Flechtschritte sind:

1. Strang 2 über 1; 4 über 3; 6 über 5 und 0 über 7
1. Strang 3 unter 2; 5 unter 4; 7 unter 6 und 1 unter 0

<img src='{{ "/img/challa/braiding.png" | relative_url }}' class="full-width" />

Danach wieder mit Schritt eins beginnen.
In jedem Schritt wird der untere Strang jeweils über den Oberen gelegt,
so dass nicht viel überlegt werden muss.
Nach 2 bis 3 Wiederholungen (4-6 mal rundherum Flechten)
die Enden unter der Challa verstecken und
diese auf dem mit Backpapier belegten Backblech platzieren.


### Backen

Die Challot wiederum etwas zwei Stunden gehen lassen;
bis ein leichtes Eindrücken des Teigs mit dem Finger eine Delle hinterlässt.

Den Ofen auf 180 °C vorheizen.
Die Challot mit dem Eigelb bestreichen und mit den zerkleinerten Kürbiskernen bestreuen.
Nun müssen sie 30 min - 45 min bei 180 °C im Ofen gebacken werden.
Anschliessen abkühlen lassen.

## Links

* [Julie Remer, _braiding a round challah_, YouTube, 23. Sep, 2016](https://youtu.be/kvbW4zMpzB4)
* [panissimo - Rezept des Monats, _Zweierlei-Brot mit Raps- & Kürbiskernöl_, 15.
  Februar 2017,](https://www.swissbaker.ch/de/panissimo-news/themenarchiv/rezept-des-monats/rezept-des-monats-details/zweierlei-brot-mit-raps-kuerbiskernoel/)
* [Dave Richeson. _The maypole braid group_, 4. May, 2009](https://divisbyzero.com/2009/05/04/the-maypole-braid-group/)
