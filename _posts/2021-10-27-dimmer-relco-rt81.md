---
layout: post
lang: en
title: Cursor replacement for the floor dimmer Relco RT81
tags: hardware
date: 2021-10-27 20:56 +0200
---
The [Relco RT81 dimmer](https://store.relcogroup.com/prodotto/interruttore-a-scorrimento-da-pavimento-comando-a-cursore-rt81-led-n-cod-rl1104-led/)
of a secondhand lamp had a missing cursor. Thus
the regulation by foot was not so good.

![Relco with missing cursor]({{ "/img/relco/img1.jpg" | relative_url }} )

A replacement was designed in [FreeCAD](https://www.freecadweb.org/) and 3D
printed in black PLA.

![replaced]({{ "/img/relco/img2.jpg" | relative_url }} )

You may use the following files to create your own replacement:

* [relco-rt81-cursor.FCStd]({{ "/files/relco/relco-rt81-cursor.FCStd" | relative_url}})
* [relco-rt81-cursor.stl]({{ "/files/relco/relco-rt81-cursor.stl" | relative_url}})

The original part (which was not available) was a bit wider, possibly to
improve stability but no problems have been observed with the replacement so
far (1 week).
