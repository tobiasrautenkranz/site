---
layout: post
title: KDE Gemini KIO & KPart Plugin
category: code
tags: code
date: 2021-04-26 21:36 +0200
---
A [Gemini](//gemini.circumlunar.space/) internet protocol client integration for
[KDE](https://kde.org).

It allows fetching and viewing (using _kio-gemini_ and _geminipart_) Gemini
documents mainly in [Konqueror](https://apps.kde.org/konqueror/).
Using _kio-gemini_ all KDE applications using
[KIO](https://community.kde.org/KIO) are able to access files
using the Gemini Protocol (e.g.: fetch atom feeds with Akregator).
Applications using
[KParts](https://api.kde.org/frameworks/kparts/html/index.html) can display
rendered Gemini text files.

## Install

The two necessary plugins are available as source code:

* <https://gitlab.com/tobiasrautenkranz/geminipart>
* <https://gitlab.com/tobiasrautenkranz/kio-gemini>

Given the necessary KDE & Qt development files, you should be able to install
them with the usual cmake and make commands:

```
cmake .; make; sudo make install
```

## Usage

Now you can use Konqueror to browse the Gemini Space.
![Screenshot Konqueror]({{ "/img/kde_geminipart.png" | relative_url }})

You may have to chose the correct KPart in Konqueror:
> View : View Mode : Gemini View
