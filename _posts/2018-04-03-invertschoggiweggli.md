---
layout: post
title: Invertschoggiweggli
lang: de
category: recipes
tags: recipe bread
---
![Invertschoggiweggli](
{{ "/img/invertschoggiweggli/invertschoggiweggli.jpg" | relative_url }})  
Weisse Schokolade – Schwarzes Weggli

## Zutaten

Für acht Weggli oder 16 kleine Weggli (ca. ein Blech)

|Menge  | |
|------:|-----------|
| 500 g | Weissmehl |
| 350 g | Milch     |
|  50 g | Butter    |
|   3 g | Hefe      |
|  10 g | Salz      |
|   6 g | Zucker    |
|  ~4 g | Schwarze Lebensmittelfarbe  |
| 132 g | Weisse Schokolade   |

## Zubereitung

Die Milch in eine Schüssel geben. Die Hefe, den Zucker
und die Lebensmittelfarbe untermischen.
Das Mehl beigeben und mischen, bis es sich mit der Flüssigkeit verbunden hat.
Anschliessend den Teig gut kneten.
Butter und Salz stückweise beim weiteren Kneten einarbeiten.
Nun den Teig flach ausbreiten, Schokoladestücke darauf platzieren
und anschliessen einfalten;
Wiederholen, bis die Schokolade verarbeitet ist.

Nun den Teig gehen lassen, bis sich sein Volumen verdoppelt hat (~ 2&nbsp;h).

Anschliessend den Teig zu acht (bzw. 16) ovalen Teigstücken formen;
dabei die Haut des Teigs gut spannen.
Nochmals 1.5 – 2 Stunden gehen lassen.
Mit einer Schere der Längsachse entlang Zacken einschneiden
(ca. im 45° Winkel) und mit Öl bestreichen.
20 – 25&nbsp;min bei 180&nbsp;°C im Ofen backen. Abkühlen lassen.

## Normale Schoggiweggli

Mit schwarzer Schokolade und ohne Lebensmittelfarbe können auch
gewöhnliche Schoggiweggli gebacken werden.
Für eine schöne Farbe sollten diese mit Eigelb statt Öl bestrichen werden.

## Farbe

Zum Färben wird schwarze Lebensmittelfarbe verwendet (_E 153_). Alternativ kann auch
Ativkohle (_Carbo activatus_) verwendet werden.
Siehe:
[**Purgatory Burgers**.
spunk. _instructables_. 10. November, 2014
](http://www.instructables.com/id/Purgatory-Burgers/).
