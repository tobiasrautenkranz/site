---
layout: post
title: Moby writes a poem
date: 2024-06-19 10:31 +0200
tags: code
---

> 🤖❤️🧅
>
> — Moby

# אַ ראָבאָט װאָס שרײַבט לידער

The poem written by robot Moby (מאָבי) at the end of
[lesson 6-4 of yiddishpop](https://yiddishpop.com/kapitl6/lektsye4)
(אַ ראָבאָט װאָס שרײַבט לידער) is as follows:

```
11110000 10011111 10100100 10010110
         11100010 10011101 10100100
         11101111 10111000 10001111
         11110000 10011111 10100111
                           10000101

מאָבי —
```

Directly
[converting from binary](
https://gchq.github.io/CyberChef/#recipe=From_Binary('Space',8)&input=MTExMTAwMDAgMTAwMTExMTEgMTAxMDAxMDAgMTAwMTAxMTAKICAgICAgICAgMTExMDAwMTAgMTAwMTExMDEgMTAxMDAxMDAKICAgICAgICAgMTExMDExMTEgMTAxMTEwMDAgMTAwMDExMTEKICAgICAgICAgMTExMTAwMDAgMTAwMTExMTEgMTAxMDAxMTEKICAgICAgICAgICAgICAgICAgICAgICAgICAgMTAwMDAxMDEK&oenc=65001&oeol=NEL)
(no right-to-left as suggested by the alignment) we
get the UTF-8 encoded text:

```
🤖❤️🧅
```

with `U+1F916 ROBOT FACE` in the first line.
The heart spans two next lines with a code point each:
`U+2764 HEAVY BLACK HEART` and
`U+FE0F VARIATION SELECTOR-16`.
The last two lines (4 bytes) are the onion character:
`U+1F9C5 ONION`.

You can see the number of bytes per codepoint by counting the number
of leading ones. The successive bytes will start with `10`. E.g.
for 🤖 we have four leading ones in the first byte `1111000`. Thus,
the code point spans the four bytes of the first line.
The next byte on the second line starts the next code point with three leading
zeros, and thus the three bytes of the second line.

The onion may be a reference to the start of
[lesson 3-3](https://yiddishpop.com/kapitl3/lektsye3)
where Moby already likes onions.
