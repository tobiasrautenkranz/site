---
layout: post
title: Luise von Himmelbetts Brot
category: recipes
tags: recipe bread
date: 2023-09-27 14:32 +0200
---
> The smell of the brown loaves was like fidelity.

Hoban, Russell. _The Medusa Frequency_. 1987

# Rezept: Luise von Himmelbetts Brot

Angepasstes Rezept des Original (siehe unten) für ein Sandwich- / Toastbrot.

| Weissmehl | 550 g | 73 %
| Vollkornmehl | 100 g | 13 %
| Haferflocken | 100 g | 13 %
| Gerstenmalzmehl | 20 g | 2 %
| Kümmel | 5 g | 0.6 %
| Salz | 13 g | 1.7 %
| Trockenhefe | 7 g | 1 %
| Öl (Sonnenblumen) | 8 g | 1 %
| Wasser (warm) | 425 g | 57 %

1x Brot/Cakeform ca. 35 &times; 10 &times; 7 cm
(Länge &times; Breite &times; Höhe)

Alle trockenen Zutaten in einer Schüssel mischen, Öl und Wasser hinzufügen.
Gut mischen und dann den Teig 10-15 Minuten kneten.
In der zugedeckten Schüssel an einem warmen Ort gehen lassen
bis der Teig doppelt so gross ist (1 - 2 h).

Teig aus der Schussel nehmen und mit Schlagen und Drücken
grosse Luftblasen entfernen. Zu einem zylinderförmigen Leib
formen, so das dieser in die gebuttert Form passt.

![Brot in der Form]({{ "/img/medusa-frequency-bread/bread_pan.jpg" | relative_url}})

Wiederum zugedeckt  gehen lassen, bis der Teig doppelt so gross ist.
Im vorgeheizten Ofen bei 220°C 40 bis 45 Minuten backen.
Aus der Form nehmen und auf eine Gitter abkühlen lassen.

![Brot]({{ "/img/medusa-frequency-bread/bread.jpg" | relative_url}})

## Notizen

Der _granary flour_ hat die folgenden
[Zutaten](https://www.waitrose.com/ecom/products/hovis-granary-bread-flour/476719-729900-729901):

> Wheat Flour (with added Calcium, Iron, Niacin, Thiamin),
> Malted Wheat Flakes (16%), Wheat Protein, Malted Barley Flour

An diesen habe ich mich für das Mehl orientiert; Aber Haferflocken und
Gerstenmalzmehl verwendet, da diese einfacher erhältlich sind.

Die Trockenhefe gebe ich immer direkt zum Mehl, ein Anrühren mit Wasser und
Zucker ist nicht nötig.

## Original Rezept

```
1.5 kg granary flour
2 dessertsp oil
1   ''      salt
1 tblesp caraway seeds
2   ''   dried yeast
1½ pts water, blood warm
1 teasp sugar
```

put flour in a bowl, add oil & caraway seeds. put sugar &
yeast in a jug, add a little of the warm water. leave for
10-15 mins in a warm place to froth, add salt to warm
water. when yeast dissolved, add to the flour and water.
stir, then turn on to a floured board & knead 10-15 mins
until it is elastic. put back in bowl, cover, leave to rise in
warm place. when doubled in size, take out, divide into 2,
knead & thump, shape into loaves and put in greased tins.
cover, leave for 10 mins in a warm place, then put in oven
& bake at 220° for 40-5 mins.
