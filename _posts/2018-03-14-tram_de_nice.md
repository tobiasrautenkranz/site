---
layout: post
title: Le Tram de Nice
lang: de
category: code
tags: code
---
Side-Scroller Computerspiel zum [Tram der Linie 1 in Nizza](https://fr.wikipedia.org/wiki/Tramway_de_Nice).

[![Screenshot]({{ "/img/tram_de_nice/screenshot.jpg" | relative_url }})](/tram_de_nice)

Ziel ist die letzte Haltestelle möglichst schnell zu erreichen.
Dazu sollte der Akku des Trams bei jeder Haltestelle nur so weit geladen werden,
wie dies zum Erreichen der nächsten Haltestelle nötig ist.

Du kannst es online [spielen](/tram_de_nice).


Das Spiel ist in _ECMAScript 6_ programmiert.
Der [Source Code](https://gitlab.com/tobiasrautenkranz/tram_de_nice)
ist frei verfügbar.

