---
layout: post
title: Universität Basel Vorlesungszeiten Frühjahrsemester 2019
lang: de
category: data
tags: data
---

## Zeitliche Verteilung der Uni Basel Lehrveranstaltungen

Die
[Lehrveranstaltungen der Universität Basel](https://vorlesungsverzeichnis.unibas.ch)
finden zu verschiedensten Zeiten statt. Aufgeschlüsselt nach Fakultäten
ergibt sich für das Frühjahrsemester 2019 bezüglich
dem Wochentag respektive der Uhrzeit folgende relative Verteilung:

<img src='{{ "/img/unibas-vorlesungen/by_department-week.png" | relative_url }}' class="full-width" />

<img src='{{ "/img/unibas-vorlesungen/by_department-time.png" | relative_url }}' class="full-width" />

Sortiert, so dass weiter unten _besser_ (i.e.: mehr "Vorlesungsfreie Zeit") ist.

## Bachelor

Für die Unterteilung in Studienfächer sind nur die Bachelor-Angebote
berücksichtigt, da diese über eine hinreichende Anzahl von Veranstaltungen
verfügen.
Zur Bewertung wurde ein _Chill_ Faktor berechnet welcher mit absteigender
Wichtung, die Länge des Wochenendes, später Begin, früher Schluss und langer
Mittagspause bewertet. Also grösser Faktor = besser.
<img src='{{ "/img/unibas-vorlesungen/chill_bachelor.png" | relative_url }}' class="full-width" />

Abgesehen von der Subjektivität Wichtung, entsprechen die zugrundeliegenden Daten
nicht einem tatsächlichen oder zu erwartendem Studium. So werden
Veranstaltungen welche mehrfach durchgeführt werden (e.g. Übungen) mehrfach
gezählt; wird eine Veranstaltung unregelmässig beziehungsweise nicht wöchentlich
durchgeführt, wird dies ebenfalls falsch erfasst (ganz abgesehen vom Freiwahlbereich).

### Herbstsemester 2018
Weiter zeigt der Vergleich mit dem vorherigen Herbstsemester 2018, dass die
Wertung meist stark variiert:
<img src='{{ "/img/unibas-vorlesungen/chill_bachelorhs18.png" | relative_url }}' class="full-width" />


In Anbetracht der Unsicherheiten wird auf ein Ranking verzichtet; aber vielleicht
habt Ihr ja eine besser Idee 😜


## Details
[Alle FS 2019 Plots als pdf]({{ "/img/unibas-vorlesungen/fs2019.pdf" | relative_url }})

### Code
Die Daten stammen von PDF Datei je Abschluss aus dem Vorlesungsverzeichnis der
Universität Basel. Die Daten wurden aus dem Text der PDF Dateien extrahiert und
mit [R](https://www.r-project.org/) analysiert. Siehe den Code auf:  
[https://gitlab.com/tobiasrautenkranz/unibas-vorlesungen/](https://gitlab.com/tobiasrautenkranz/unibas-vorlesungen/)

### Daten
* [2019fs.csv](https://gitlab.com/tobiasrautenkranz/unibas-vorlesungen/blob/master/2019fs.csv)
* [2018hs.csv](https://gitlab.com/tobiasrautenkranz/unibas-vorlesungen/blob/master/2018hs.csv)
