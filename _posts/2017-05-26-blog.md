---
layout: post
title: Blog
category: code
tags: jekyll site
---
I have a new [Jekyll](https://jekyllrb.com) based blog.

This was necessary since the old site was based on ancient technology; i.e.:
[M4](https://en.wikipedia.org/wiki/M4_(computer_language))
and [GNU Bazaar](https://en.wikipedia.org/wiki/GNU_Bazaar).
Additionally, there was no support for blog posts.

Thus, this blog was created using the old css file for a similar look. The code is
available at [GitLab](https://gitlab.com/tobiasrautenkranz/site).

The old stuff is still [here](/code.html)

## Todos

* Better handling of images (responsive + thumbnails)
* Whitespace in generated html (fix in
  [Jekyll 4](https://github.com/Shopify/liquid/pull/773))
