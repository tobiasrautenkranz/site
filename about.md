---
layout: page
title: about
permalink: /about/
---

# Tobias Rautenkranz

📧 <a href="&#109;ailto:&#109;ail&#x40;t&#x6f;bias.rautenkranz.ch">&#109;ail&#x40;t&#x6f;bias.rautenkranz.ch</a><br>
🔑 [GPG Key](/tobias_rautenkranz.asc)
